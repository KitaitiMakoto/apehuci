const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, 'source/javascripts/libs.js'),
  output: {
    filename: 'libs.js',
    path: path.resolve(__dirname, '.tmp/dist/javascripts')
  }
};
