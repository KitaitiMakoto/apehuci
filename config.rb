###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false
page '/*.atom', layout: false

# With alternative layout
# page "/path/to/file.html", layout: :otherlayout

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", locals: {
#  which_fake_page: "Rendering a fake page with a local variable" }

###
# Helpers
###
helpers do
  include Erubis::XmlHelper

  def encode_url_path_segments(path)
    path.split('/').map {|segment| url_encode(segment)}.join('/')
  end
  alias upath encode_url_path_segments
end

activate :relative_assets
set :relative_links, true

set :markdown, 'syntax_highlighter' => 'rouge'

# Reload the browser automatically whenever files change
configure :development do
#   activate :livereload
  config[:base] = '/'
end

# Methods defined in the helpers block are available in templates
# helpers do
# end

# Build-specific configuration
configure :build do
  config[:base] = '/'
  config[:force_tls] = true

  activate :minify_html do |html|
    html.remove_intertag_spaces = true
    html.simple_doctype = true
    html.remove_form_attributes = true
    html.remove_quotes = true
  end
end

activate :asciidoc, attributes: ["toc=macro", "source-highlighter=rouge"]

activate :external_pipeline,
  name: :webpack,
  command: build? ? "npm run build" : "npm run watch",
  source: ".tmp/dist",
  latency: 1

activate :blog do |blog|
  Time.zone = 'Tokyo'
  blog.sources = '{year}/{month}/{day}.html'
  blog.permalink = '{year}/{month}/{day}.html'
  blog.layout = 'blog'
  blog.tag_template = 'tag.html'

  blog.paginate = true
  blog.per_page = 3

  blog.summary_generator = ->(article, rendered, length, ellipsis) {
    content = Nokogiri.HTML(rendered).content.gsub(/\s+/, ' ')

    summary = content[0..length]
    summary << ellipsis unless content == summary
  }
end

require 'lib/feed'
activate :feed do |feed|
  feed.uri = 'recent-days.atom'
end
