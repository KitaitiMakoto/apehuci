---
title: Web Speech APIのバグ
tags:
  - Web Speech API
---

Web Speech APIのSynthesis（合成音声での読み上げ）使ってて、読み上げがが終わった(止まった)時にイベントが発生するのだけどそれが最後まで読み終わった物なのかキャンセルされた物なのか知りたい。と思って仕様を調べたら、読み終わった場合は `end` イベントが発生して、キャンセルされた場合はエラーになるのでそれにコールバックを登録すればよさそう。に見えるのだけどやってみるとうまくいかない。キャンセル時にエラーになってくれない。これはブラウザーのバグ？　てかそもそもまだドラフト段階だから実装後に仕様が変わったのかな、と思ってBugZillaを見てみたら案の定だった。詳しくはこちらをどうぞ： +
link:https://scrapbox.io/apehuci/Web_Speech_Synthesis_Utterance%E3%81%8C%E6%99%AE%E9%80%9A%E3%81%AB%E7%B5%82%E3%82%8F%E3%81%A3%E3%81%9F%E3%81%AE%E3%81%8Bcancel()%E3%81%A7%E6%AD%A2%E3%81%BE%E3%81%A3%E3%81%9F%E3%81%AE%E3%81%8B%E7%9F%A5%E3%82%8A%E3%81%9F%E3%81%84[Web Speech Synthesis Utteranceが普通に終わったのかcancel()で止まったのか知りたい]
