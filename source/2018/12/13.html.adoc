---
title: Web Speech API
tags:
  - JavaScript
---

数年ぶりにlink:https://developer.mozilla.org/docs/Web/API/Web_Speech_API[Web Speech API]を調べたらFirefoxでも使えるようになっていました。「このAPI楽しいけど世間の関心薄そうだしChromeだけで入っててそのうち消えるのかなあ」って感じていたので嬉しい。このまま広まるんだろうか。
